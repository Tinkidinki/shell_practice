import os
import tornado.web
import tornado.websocket
import tornado.httpserver
import tornado.ioloop

# The web socket class
class WebSocketHandler(tornado.websocket.WebSocketHandler):
    def open(self):
        pass

    # This gets called when the server end of the socket receives a message.
    def on_message(self, message):
        self.write_message(u"Your message was: " + message)
    
    def on_close(self):
        pass


# Class that renders the index page
class IndexPageHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html")

# Class that handles the routes of the application
class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r'/', IndexPageHandler),
            (r'/websocket',WebSocketHandler)
        ]

        settings = {
            'template_path':'templates',
            'static_path': os.path.join(os.path.dirname(__file__), "static")

        }
        tornado.web.Application.__init__(self,handlers,**settings)


# Starts the app
if __name__ == '__main__':
    ws_app = Application()
    server = tornado.httpserver.HTTPServer(ws_app)
    server.listen(1234)
    tornado.ioloop.IOLoop.instance().start()

// pre is the text that will be displayed on the terminal each time
var pre = $('pre');

// Terminal scrolling part
var scroll_object = $('body').scroll_element();
function scroll_to_bottom() {
  var sHeight = scroll_object.prop('scrollHeight');
  scroll_object.scrollTop(sHeight);
};

// ******************** THE WEBSOCKET PORTION ************************
var ws;

function onLoad() {
	ws = new WebSocket("ws://localhost:1234/websocket");
	
	// This gets called when the client end of the socket receives a message
	ws.onmessage = function(e) {
		alert(e.data);
	}
}
	
//I will have to call this with the command.
function sendMsg(command){
	ws.send(command);
} 
// *******************************************************************         

      

//The terminal command line options
$('#terminal').cmd({
	prompt: '>',
	width: '100%',
//   commands: function(command) {
//     var html = pre.html();
//     if (html) { html += '\n'; }
//     if (command == "hello"){
//       var message = "you suck"
//     }
//     else {
//           var message = "you said " + 
//           "<span style=\"color:white\">" + command +
//         "</span>" };
//     pre.html(html + '> ' + command + '\n' + message);
//     scroll_to_bottom();
//   }

	commands: function(command) {
		sendMsg(command);
	}

});